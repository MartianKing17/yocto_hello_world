LICENSE = "CLOSED"

inherit pkgconfig externalsrc cmake_qt5 systemd

DEPENDS += "qtbase qtquickcontrols2 qtquickcontrols"
SRC_URI = "git://gitlab.com/MartianKing17/qt_hello_world.git"
SRCREV = "aaf84a527a8507692a8c1fddd3e788dcd6a9cd6e"

S = "${WORKDIR}/git"
RDEPENDS_${PN} += "bash"

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "helloworld.service"
FILES:${PN} += "${systemd_unitdir}/system/helloworld.service"

do_install:append() {
  install -m 0755 ${S}/helloworld_startapp.sh ${D}/${bindir}/helloworld_startapp.sh
  install -d ${D}${systemd_unitdir}/system
  install -m 0644 ${S}/helloworld.service ${D}${systemd_unitdir}/system/helloworld.service
}

